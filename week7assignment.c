//
//  main.c
//  week7
//
//  Created by Mario Pirollo on 10/24/17.
//  Copyright © 2017 Mario Pirollo. All rights reserved.
//

#include <stdio.h>
int count=0;
int quiz(void);
int main(void) {
    int score;
    printf("Global Warming Facts Quiz \n");
    score=quiz();
    //display quiz score
    printf("Your score is %d\n",score);
    //evaluating quiz score
    if(score==5)
        printf("Excellent");
    if(score==4)
        printf("Very Good");
    if(score<4)
    {
        printf("Time to brush up on your knowledge of Global Warming. \n Check these websites: https://climate.nasa.gov/evidence/ \n https://nrdc.org/stories/global-warming-101");
        
    }
    system("PAUSE");
    
}
int quiz(void)
{
    
    //declaring variables
    int answer1;
    int answer2;
    int answer3;
    int answer4;
    int answer5;
    //displaying question 1
    printf("Which country currently gives off the most greenhouse gas?");
    printf("\n 1)United States\n2)China ");
    printf("\n 3)India\n4)Italy");
    //user output
    scanf("%d",&answer1);
    //check if answer is correct or1 not
    if(answer1==2)
        ++count;
    else
        printf("You are incorrect! Answer is 2\n");
    //display 2nd question
    printf("How many human deaths per year does the World Health Organization attribute to climate change?");
    printf("1)10,500\n2)150,000 ");
    printf("3)1,500\n4)1,0000");
    //user output
    scanf("%d",&answer2);
    
    //check if answer is correct of not
    if(answer2==2)
        ++count;
    else
        printf("You are incorrect! Answer is 2\n");
    
    //display question 3
    printf("How long does it take for carbon dioxide in the atmosphere to disperse?");
    printf("1) 1 year\n2)10 years ");
    printf("3)65 years\n4) 100 years");
    //user output
    scanf("%d",&answer3);
    //check if answer is correct of not
    if(answer3==4)
        ++count;
    else
        printf("You are incorrect! Answer is 4\n");
    
    //display question 4
    printf("Which Arctic animal do many scientists consider most vulnerable to extinction due to global warming?");
    printf("1) Polar Bears\n2) Tigers ");
    printf("3)Narwhals\n4)Tropical Frogs");
    //user output
    scanf("%d",&answer4);
    //check if answer is correct of not
    if(answer4==3)
        ++count;
    else
        printf("You are incorrect! Answer is 3\n");
    //display question 5
    
    printf("During what time of day does air travel have the least damaging effect on the environment?");
    printf("1)Nighttime\n2)Daytime ");
    printf("3)Dusk \n4) Doesn't matter");
    //user output
    scanf("%d",&answer5);
    //check if answer is correct of not
    if(answer5==1)
        ++count;
    else
        printf("You are incorrect! Answer is 1\n");
    
    //return number of correct answers
    return(count);
    
    
    
    
    
    
    
    
    
    
}


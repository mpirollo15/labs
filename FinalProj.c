//
//  main.c
//  FinalProj
//
//  Created by Mario Pirollo on 11/02/17.
// Date Modified: 12/07/2017
// Description: A  quiz on Pittsburgh Sports Trivia
//  Copyright © 2017 Mario Pirollo. All rights reserved.
//

#include <stdio.h>
int count=0;
int quiz(void);
int main(void) {
    int score;
    printf("Yinzer Sports Trivia \n");
    score=quiz();
    //display quiz score
    printf("Your score is %d\n",score);
    //evaluating quiz score
    if(score==10)
        printf("You're a true Yinzer! Are yinz sure you aint dis guy? \n https://twitter.com/colin_dunlap/status/937347932805844992");
    
    if(score==9)
        printf("It's obvious yinz are from Pittsburgh");
    if(score<8)
    {
        printf("You's a Jagoff! Do you even live in the Burgh?");
        
    }
    system("PAUSE");
    
}
int quiz(void)
{
    
    //declaring variables
    int answer1;
    int answer2;
    int answer3;
    int answer4;
    int answer5;
    int answer6;
    int answer7;
    int answer8;
    int answer9;
    int answer10;
    //displaying question 1
    printf("How many Super Bowls have the Steelers played in?");
    printf("\n 1)6 \n 2)5 ");
    printf("\n 3)8 \n 4)10");
    //user output
    scanf("%d",&answer1);
    //check if answer is correct or1 not
    if(answer1==3)
        ++count;
    else
        printf("Yinz are incorrect! Answer is 3\n");
    //display 2nd question
    printf("What year was Pittsburgh Penguins center, Sidney Crosby drafted?");
    printf("\n 1)2005 \n 2)2002 ");
    printf("\n 3)2008 \n 4)2007");
    //user output
    scanf("%d",&answer2);
    
    //check if answer is correct of not
    if(answer2==1)
        ++count;
    else
        printf("Yinz are incorrect! Answer is 1\n");
    
    //display question 3
    printf("What year did the former home of the Pittsburgh Penguins, the Mellon Arena, open?");
    printf("\n 1)1950 \n 2)1976 ");
    printf("\n 3)1982 \n 4)1961");
    //user output
    scanf("%d",&answer3);
    //check if answer is correct of not
    if(answer3==4)
        ++count;
    else
        printf("Yinz are incorrect! Answer is 4\n");
    
    //display question 4
    printf("Who invented the infamous Terrible Towel?");
    printf("\n 1)Mean Joe Greene \n 2)Mr. Rodgers ");
    printf("\n 3)Myron Cope \n 4)Donnie Iris");
    //user output
    scanf("%d",&answer4);
    //check if answer is correct or not
    if(answer4==3)
        ++count;
    else
        printf("You are incorrect! Answer is 3\n");
    //display question 5
    
    printf("Who is the Steelers all time recieving leader??");
    printf("\n 1)Antonio Brown \n 2)Hines Ward ");
    printf("\n 3)Santonio Holmes \n 4)Lynn Swann");
    //user output
    scanf("%d",&answer5);
    //check if answer is correct or not
    if(answer5==2)
        ++count;
    else
        printf("Yinz are incorrect! Answer is 2\n");
    
    //display question 6
    
    printf("When was the last time the Pitt Panthers won a National Championship in football??");
    printf("\n 1)1976 \n 2)1924 ");
    printf("\n 3)2001 \n 4)1984");
    //user output
    scanf("%d",&answer6);
    //check if answer is correct or not
    if(answer6==1)
        ++count;
    else
        printf("Yinz are incorrect! Answer is 1\n");
    
    //display question 7
    
    printf("Who is the founder of the Steelers??");
    printf("\n 1)Arthur J. Rooney \n 2)Dan Rooney ");
    printf("\n 3)Bill Cowher \n 4)Chuck Noll");
    //user output
    scanf("%d",&answer7);
    //check if answer is correct of not
    if(answer7==1)
        ++count;
    else
        printf("Yinz are incorrect! Answer is 1\n");
    
    //display question 8
    
    printf("What were the Steelers called before they were named the Steelers??");
    printf("\n 1)They were always the Steelers \n 2)Pirates ");
    printf("\n 3)Bandits \n 4)Penguins");
    //user output
    scanf("%d",&answer8);
    //check if answer is correct of not
    if(answer8==2)
        ++count;
    else
        printf("Yinz are incorrect! Answer is 2\n");
    
    //display question 9
    
    printf("How many World Series do the Pirates have??");
    printf("\n 1)0 \n 2)3 ");
    printf("\n 3)7 \n 4)5");
    //user output
    scanf("%d",&answer9);
    //check if answer is correct of not
    if(answer9==4)
        ++count;
    else
        printf("Yinz are incorrect! Answer is 3\n");
    
    //display question 10
    
    printf("Who is the youngest player on the Steelers roster??");
    printf("\n 1)TJ Watt \n 2)Ben Roethlisberger ");
    printf("\n 3)Juju Smith Schuster)7 \n 4)Ryan Shazier");
    //user output
    scanf("%d",&answer10);
    //check if answer is correct of not
    if(answer10==3)
        ++count;
    else
        printf("Yinz are incorrect! Answer is 3\n");
    
    //return number of correct answers
    return(count);
    
    
    
    
    
    
    
    
    
    
    
}

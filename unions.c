//
//  main.c
//  Unions
//
//  Created by Mario Pirollo on 11/7/17.
//  Copyright © 2017 Mario Pirollo. All rights reserved.
//

#include <stdio.h>
#include <string.h>

union MyDataGame {
    int points;
    float quarter;
    char str[120];
    
};

int main() {
    union MyDataGame data;
    
    data.points = 25;
    data.quarter =4.0;
    strcpy(data.str, "My Demo Game");
    
    printf("Points : %d\n", data.points);
    printf("Quarter : %d\n", data.quarter);
    printf("Data : %s\n", data.str);
    
    return 0;
    
}

#include <stdio.h>

//function main begins program execution
int main ( void )
{
    // get user's miles driven
    int miles; // miles driven
    printf(  "%s", "Please enter total miles driven per day (in miles): " );
    scanf( "%d", &miles );
    
    // cost of a gallon of gas
    int cost ; // gas cost
    printf( "Please enter the cost of gas per gallon (in dollars): " );
    scanf( "%d", &cost );
    
    // users avg MPG
    int mpg ; // miles per gallon average
    printf( "Please enter your average miles per gallon (MPG): " );
    scanf( "%d", &mpg );
    
    // parking fees in area
    int parking ; // daily parking fees
    printf( "Please enter your daily parking fees (in dollars): " );
    scanf( "%d", &parking );
    
    // tolls per day
    int tolls ; // daily tolls
    printf( "Please enter your daily tolls(in dollars): " );
    scanf( "%d", &tolls );
    
    int dailyCost; // user's daily driving cost
    dailyCost = miles / mpg * cost + parking + tolls; // calculate dailyCOst
    
    printf( "Your daily cost of commuting is $%d\n\n", dailyCost ); // output daily cost
    
    
}

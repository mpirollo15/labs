//
//  main.c
//  forloop
//
//  Created by Mario Pirollo on 10/5/17.
//  Copyright © 2017 Mario Pirollo. All rights reserved.
//

#include <stdio.h>

int main(void){
    //declare variables
    int score;
    
    //for loop
    for(score=1; score<=17; score+=6){
        printf("the score of the game is %d \n", score);
        
    }
    return 0;
    
}

